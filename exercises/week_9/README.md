# Week 9 (July 16-22 )
- Transposable Elements / Repeats
- Pangenome continued
- Tubeworm biology

## Readings
On July 23 we'll have a presenter, Jess Storer, during the 10-11am time slot in place of journal club. Please read these papers to prepare for her presentation.
1. [A Field Guide to Eukaryotic Transposable Elements](https://www.annualreviews.org/content/journals/10.1146/annurev-genet-040620-022145)
2. [On the genetic basis of tail-loss evolution in humans and apes](https://www.nature.com/articles/s41586-024-07095-8)

Samira and Sia will also lead journal club for this paper on the Mediterranean mussel:  
3. [Massive gene presence-absence variation shapes an open pan-genome in the Mediterranean mussel](https://link.springer.com/article/10.1186/s13059-020-02180-3)

## Exercises

### 1. Continue with Riftia assemblies

Prepare to share your results on your group git README (including scripts, relevant summary tables, and figures).
- Include descriptions on both what happened and also your reflections on the outcomes (bad/good, why?) 
- Here is an example template for this: https://gitlab.com/PlantGenomicsLab/fraxinus-genome-assemblies

Click on the "Details" button below to see assembly guidance:  

<details>

##### 1. Finish rebasecalling Riftia data

> **riftia-1_AD4317** - Andrew & Sia <br>
> **riftia-5_AD4103** - Alan & Samira

**Important Notes:** You can find the software as a module file on Xanadu. To check, type `module avail` into the terminal. To load the software, you will need to add `module load <tool>/<version>` to the top of your bash script.

- **A. Prepare and maintain the git and Xanadu working space**
    - This is the main directory / git repository: `/core/projects/EBP/conservation/tubeworms/`
    - Within there, you'll find:
        - `genome_assembly/rifPach/riftia-1_AD4317`  
        - `genome_assembly/rifPach/riftia-5_AD4103` 
    - Please refer to `genome_assembly/rifPach/reference` for general folder structure and README (noting things are done slightly differently in here - e.g. no rebasecalling)
    - This is the gitlab page: [https://gitlab.com/mneitzey/tubeworms](https://gitlab.com/mneitzey/tubeworms), which I have added everyone to  
    - Please make use of `/core/projects/EBP/conservation/tubeworms/.gitignore` - there will probably be file types / names that need to be added. The git is really only for scripts and READMEs, all other files should be ignored  
- **B. Convert fast5s to pod5s**
    - [Pod5 converter](https://github.com/nanoporetech/pod5-file-format)
    - Fast5 Data
        - riftia-1_AD4317
            - This run was restarted *and* there were "skipped" fast5 files, so there are actually 4 folders to pull fast5s from:
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_skip`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_skip`
        - riftia-5_AD4103
            - This run is simple, 1 folder :) 
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample3/nanopore/2023JAN17_DOG_Rif_5_A4103_PAM25110/2023JAN17_DOG_Rif_5_A4103_PAM25110/20230117_2214_1F_PAM25110_a8900295/fast5_pass`
- **C. Re-basecall with Dorado** 
    - [Dorado basecaller](https://github.com/nanoporetech/dorado)
    - Parameters
        - `--model dna_r10.4.1_e8.2_400bps_sup@v5.0.0`
        - `--modified-bases 5mCG_5hmCG`
        - `--min-qscore 10`
- **D. Convert bam to fastq**
    - [bamtofastq](https://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html)
    - Dorado outputs a bam, which contains methylation that we'll want *later*, but for genome assembly we need the fastqs
- **E. Assess read stats with NanoPlot**
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)
    - Download the report.html and check it out!  

##### 2. Genome assembly with Riftia data
Moving forward, the genome assembly will be split between two methods: one person will decontaminate the reads moving into assembly, and the other will go into assembly with all reads (skip F & G, but help your partner) and decontaminate the contigs at the end of assembly.  
- **F. Identify contaminant reads with Centrifuge**
    - [Centrifuge](https://ccb.jhu.edu/software/centrifuge/manual.shtml)
    - Special parameters
        - `-x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v` - includes bacteria, archael, viral, and human databases  
        - `--exclude-taxids 9606` - to exclude human, since this can throw out metazoan reads  
        - `--min-hitlen 40` - sets minimum base match length to 40
    - Use Recentrifuge on the `.out` file to visualize classification results 
        - [Recentrifuge](https://github.com/khyox/recentrifuge)
- **G. Remove contaminants (classified reads) with Seqkit grep & QC reads with Nanoplot**
    - [Seqkit](https://bioinf.shenwei.me/seqkit/usage/#grep)
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)    
- **H. Assemble genome with Flye**
    - [Flye](https://github.com/mikolmogorov/Flye)
    - Special parameters   
        - `--nano-hq` - since reads were rebasecalled with Dorado
        - `--scaffold`
        - `--no-alt-contigs`
        - `--asm-coverage 60`
- **I. QC genome**
    - Contiguity: [QUAST](https://github.com/ablab/quast)
    - Orthologous gene completeness: [BUSCO](https://busco.ezlab.org/busco_userguide.html)
        - Use `-l /core/labs/Oneill/mneitzey/databases/busco_downloads/lineages/metazoa_odb10` for database
    - Orthologous gene completeness: [Compleasm](https://github.com/huangnengCSU/compleasm)
        - Use singularity file `/core/labs/Oneill/mneitzey/Software/compleasm-0-2-6.sif` since a module isn't available yet
        - Use `-L /core/labs/Oneill/mneitzey/databases/compleasm_db/metazoa_odb10` for database
    - GFA visualization
        - If you have space on your computer, download the .gfa file and open in bandange ([install from here](https://rrwick.github.io/Bandage/))
    - K-mer completeness and correctness: [Merqury](https://github.com/marbl/merqury)
        - Merqury is confusing, learn about it watching this video (can skip trio / diploid parts): [https://www.youtube.com/watch?v=F2wsXEnMP0U](https://www.youtube.com/watch?v=F2wsXEnMP0U)
        - i. Build a k-mer database from the reads using Meryl - you only need to do this once!  
            - Get the gist for how to build one [here](https://github.com/marbl/merqury/wiki/1.-Prepare-meryl-dbs)
            - Copy `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/merqury_dbs/ont_meryl_db.sh` for template script 
        - ii. Run merqury (available on Xanadu)  

##### 3. Genome assembly polishing
Polishing is supposed to improve the base-level accuracy of the genome by mapping the reads back to the assembly and re-calling the most reprepresented base. Lately in our labs polishing hasn't improved assemblies, but this is slightly older data so it's worth a try. The polished assemblies will be compared with the unpolished, and we will decide which (polished or unpolished) to move forward into purging haplotigs.
- **J. Polish genomes with Medaka**
    - [Medaka](https://github.com/nanoporetech/medaka)
    - `--model r1041_e82_400bps_sup_v4.1.0`  
- **K. QC polished genomes**
    - Same steps as I
- **L. Organize results & update README**
    - Organize the QC results between all assemblies so they are easy to compare, we often use Google Sheets
    - Update the README to reflect the steps you've taken so far. See [here](https://gitlab.com/mneitzey/tubeworms/-/blob/master/genome_assembly/rifPach/reference/README.md?ref_type=heads) for an example

##### 4. Genome assembly purging  
To remove extreneous haplotypes from the assembly we'll run Purge Haplotigs.
- **M. Chose whether to move forward with unpolished or polished assemblies**
    - Discuss with your partner then share your QC data and thoughts in the team chat. Michelle will respond to confirm
- **N. Load R dependency for purge haplotigs**
```
# Start an interactive session in Xanadu and load R
module load R/4.2.2

# this will start an R session, which responds to different commands than linux
R

# Install ggplot2. I think you may get asked something about a time zone... I usually just put in whatever is closest (Iowa or something?)
install.packages('ggplot2')

# How to quit R and return to regular linux
quit()
```
- **O. Run Purge Haplotigs**
    - [Purge Haplotigs](https://bitbucket.org/mroachawri/purge_haplotigs/src/master/)
    - [Purge Haplotigs publication](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2485-7)
    - This consists of 4 steps, starting with mapping the reads used for the assembly (same you would've used for polishing) to the assembly. An important part is after step 2 you'll download the histogram and decide on the coverage cutoffs for low middle and high
    - Find scripts here: `/core/labs/Oneill/mneitzey/Scripts/purge_haplotigs`
- **P. QC purged assemblies**
    - Same steps as I  
</details>

### 2. Pangenome assembly practice 
Prepare to share your results and questions with the group.  

We'll likely use Minigraph-cactus to build a pangenome graph for the Riftia assemblies, so you are going to practice building a pangenome and looking at the results.  
- Using `/core/labs/Oneill/mneitzey/Software/cactus-2-8-4-gpu.sif` run the [QuickStart](https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md#quick-start) version of minigraph-cactus with the primate data. You'll also need to `git clone https://github.com/ComparativeGenomicsToolkit/cactus.git` to access the primate data  
- Use the [halTools summary information tools](https://github.com/ComparativeGenomicsToolkit/hal?tab=readme-ov-file#summary-information) to learn about the pangenome you created
- Try at least one method to visualize the pangenome graph - https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md#visualization

### 3. Understanding HPC resource use efficiency
1. gather the .out files from your job submissions into a python list. The following will collect these files, and load useful code for us to explore resource efficiency together in class. Use a new jupyter notebook and enter/execute the following (and remember to save notebook afterward executing everything):
    - manually create a list of directories that contain your .out files
    ```python
    dirs = ['/path/to/a/dir', '/path/to/another/dir', '/path/to/yet/another/dir']
    ```
    - then for each directory get a list of outfiles
    ```python
    import os

    outfiles = []
    for d in dirs:
       files = os.listdir(d)
       for basename in files:
           if basename.endswith('.out'):
               outfiles.append(f'{d}/{basename}')
    
    print(len(outfiles))

    ```
2. Clone Brandon's python repo. In terminal:
```bash
mkdir -p $HOME/src/github
cd $HOME/src/github

git clone https://github.com/brandonlind/pythonimports.git
```
3. Add the path to Brandon's repo so python can find it. In your `$HOME/.bashrc` add the following line:
```bash
export PYTHONPATH="${PYTHONPATH}:$HOME/src/github/pythonimports"
```
4. Make sure you can import Brandon's python modules.
- open a brand new terminal (this will source your ~/.bashrc so that the PYTHONPATH is updated) - no need to srun/salloc resources, we can use the login node to test the following (normally you would launch your jupyter notebook from a compute node by running `.notebook.sh` that we created in a previous week):
- activate the python conda environment: `conda activate py312`
- in terminal type `python` and hit enter (this will load a python interactive session)
- once in python, type `import myslurm as ms`. If this gives you an error, let Brandon know and we can troubleshoot.
    - one error you might see is for packages we have not yet installed - eg `ModuleNotFoundError: No module named 'tqdm'`. To fix this, open a new window, activate the py312 environment, then from the command line (not within python) `pip install tqdm`. Do this for other `ModuleNotFoundError`s. Eventually the `import myslurm as ms` within python should proceed without error

### 4. Riftia project biological questions  
For July 30 - Begin prep on the BIOLOGY of marine tubeworms and riftia in the scope of genome level questions - each genome group (Andrew & Sia; Alan & Samira) will present their ideas referencing relevant papers on questions they might want to answer from this data (or begin to look at). 