# Week 5 (June 18 - 25)

- Genome assembly methods
- Building a Gitlab repository

## Readings
1. **Journal Club -** [Evaluating long-read de novo assembly tools for eukaryotic genomes: insights and considerations](https://academic.oup.com/gigascience/article/doi/10.1093/gigascience/giad100/7449447)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper
1. [Benchmarking Oxford Nanopore read assemblers for high-quality molluscan genomes](https://royalsocietypublishing.org/doi/full/10.1098/rstb.2020.0160)
1. [Genome assembly in the
telomere-to-telomere era](genomeassembly.pdf) (Optional)

## Exercises
#### 1. Explore the markdown [guide](https://handbook.gitlab.com/docs/markdown-guide/)
- Practice the markup language using the following viewer: https://markdownlivepreview.com/
#### 2. Explore existing [project repositories](https://gitlab.com/PlantGenomicsLab)
- Take note of the 'raw' markdown code in the README.md 
- Which are easiest to follow? 
#### 3. Coding practice on Xanadu:
**Note:** There are many ways to get the answer!
> Identify software packages that are most frequently used on the Xanadu cluster

_Hint: Use grep, awk, and sed_

a. Copy `cluster_software_usage.txt` into your bcg working directory
<pre>
/core/projects/EBP/conservation/bcg/exercises/week_5/cluster_software_usage.txt
</pre>

<details><summary>Answer</summary>

`cp /core/projects/EBP/conservation/bcg/exercises/week_5/cluster_software_usage.txt $HOME/bcg`
</details>

b. Strip the version number from each software

_Example:_
<pre>
5630966|BioNetGen/2.5.0 => 5630966|BioNetGen
</pre>
- 5630966 = number of uses
- BioNetGen = software
- 2.5.0 = version number

<details><summary>Answer</summary>

`sed 's|/.*||' cluster_software_usage.txt > no_version.txt`
</details>


c. Count the unique instances of software and maintain order in the file list

_Example: zlib occurs four times in the list_

<pre>
3216|zlib/1.2.11 => 3216|zlib
1|zlib/1.2.11xanadu-03 => 1|zlib
1|zlib/1.2.11xanadu-50 => 1|zlib
1|zlib/1.2.11xaxanadu-15 => 1|zlib
</pre>
_and is used 3219 (3216+1+1+1) times._

<pre>
3219 zlib 
</pre>

<details><summary>Answer</summary>

`awk -F '|' '{ count[$2] += $1 } END { for (software in count) print count[software] "\t" software }' no_version.txt | sort -rn > unique_software.txt`
</details>

> Change FASTA headers

a. Copy `Glycine_max.fasta` into your bcg directory
<pre>
/core/projects/EBP/conservation/bcg/exercises/week_5/Glycine_max.fasta
</pre>

_Hint: You can check how many sequences are in your fasta file by using_ `grep -c ">"` _command!_


b. This protein file was downloaded from **Phytozome.org**. The sequence headers in this file look something like this:
<pre>
>Glyma.16G009500.2.p pacid=41077002 transcript=Glyma.16G009500.2 locus=Glyma.16G009500 ID=Glyma.16G009500.2.Wm82.a4.v1 annot-version=Wm82.a4.v1
</pre>

Too long! Change the headers in `Glycine_max.fasta` so they display just the **name** or **unique identifier** of the protein sequences.
<br>

_Example_:
<br>

Go from this:
<pre>
>Glyma.16G009500.2.p pacid=41077002 transcript=Glyma.16G009500.2 locus=Glyma.16G009500 ID=Glyma.16G009500.2.Wm82.a4.v1 annot-version=Wm82.a4.v1
</pre>

...to this!
<pre>
>Glyma.16G009500.2.p
</pre>

<details><summary>Answer</summary>

`sed '/^>/s/ .*//' Glycine_max.fasta > Glycine_max_modified.fasta`
or
`awk '/^>/ {$0=$1} 1'`

</details>

