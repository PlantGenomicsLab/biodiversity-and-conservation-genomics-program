11/22/24 google slides - https://docs.google.com/presentation/d/1T1xQ7PFleoqp4xlNgufIstbwhgVGnnzOCqChTjZk1zc/edit?usp=sharing  

Manuscript folder - https://drive.google.com/drive/u/0/folders/17jJrzFGsjHYaGJUgVsDwmcM0Cx2_a4_Q  

# Finalizing genome assemblies

Michelle is completing this for riftia-1 and tevnia-11

<details>

- [x] Adaptor trim with FCS-Adaptor
- [x] Decontaminate with FCS-GX
- [x] Filter out contigs/scaffolds less than 500b `seqkit seq --min-len 500 $infasta -o $outfasta`  
4. QC at every stage - BUSCO, QUAST, Merqury
5. Blobtools plots for final assembly
6. Rename final assemblies and contigs (see below)
7. Copy final renamed assembly fasta to `/core/projects/EBP/conservation/tubeworms/genome_assembly/final_assemblies` and give `chmod 555 <fasta>` permissions (read and execute for all, no writing)  

```
module load seqkit/2.8.1

seqkit sort --by-length --reverse $infasta | \
	seqkit replace --pattern '.+' --replacement 'Scaffold_{nr}' -o $outfasta
```

This will put the contigs in order from largest to smallest and rename then in that order.  

Final genomes (`$outfasta` from seqkit) will be named based on the [Darwin Tree of Life naming system](https://gitlab.com/wtsi-grit/darwin-tree-of-life-sample-naming), and thus should be named:  

* Riftia-1 : wsRifPach4.1_genome.fasta
* Riftia-5 : wsRifPach5.1_genome.fasta  
* Tevnia-4 : wsTevJeri3.1_genome.fasta 
* Tevnia-11 : wsTevJeri2.1_genome.fasta  

Each assembly git should be updated with:  
* Software versions
* All read prep and assembly steps taken
* QC at each read stage:
    * From Nanoplot: Mean and medial read length and quality (4), number of reads, read length N50, total bases  
    * Calculated coverage 
* QC at each assembly stage: 
    * All BUSCO stats
    * Mequry assembly completeness & QV  
    * From QUAST: At least # contigs, total length, GC%, N50, L50, # N's per 100kbp  
* Top (or all if reasonable) contaminants at centrifuge and FCS-GX stages  
* Purge haplotigs histogram
* Blobtools snail plot & blob plot  
* Commentary after each QC step (can be short) - what impact did the previous stage have on the genome assembly? How did this guide the next stage?  

</details>

# Genome annotation  

Continue from annotation markdown - https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/blob/main/exercises/annotation.MD - with one note below 

The TETrimmer new conda environments aren't working. Please create a conda environment from Anthony's spec-file (download Adkrb4za.txt from here).  

Installation: 
```
conda create --name TEtrimmer-AH --file Adkrb4za.txt
```

And refer to Anthony for running :)  

Samira - Riftia EASEL annotation  
- [x] Lower min mapping rate to 75%
- [x] Concatenate Luiz de Oliviera longest isoform proteins with orthodb proteins  
- [ ] Run again with latest EASEL v2.0.2-beta, same input without the regressor flag - [see slack message](https://plantcompgenomics.slack.com/archives/C03G2GBPWJG/p1731700476523799). We'll compare results

Alan - Escarpia EASEL annotation  
* ~~Try with Escarpia, Riftia, and Lamelibrachia reads - lower min mapping rate to 75% and minimum reads to 5M~~
- [x] 11/15/24 Updates
    * Run with latest EASEL v2.0.2-beta - [see slack message](https://plantcompgenomics.slack.com/archives/C03G2GBPWJG/p1731700476523799)
    * Try with Escarpia & Paraescarpia accession list (see SRA list below)
    * In params file lower min mapping `rate` to `75` & `total_reads` to `5000000`
- 11/22/24 - run with min mapping `rate 0` & `total_reads 0`

<details>
  <summary>SRA list</summary>

```
ERR12737310
SRR15321335
SRR15321336
SRR15321337
SRR15321338
SRR15321339
SRR15321340
SRR15321341
SRR15321342
SRR15321343
SRR15321344
SRR15321345
SRR15321346
SRR15321347
SRR15321348
SRR15321349
SRR15321350
SRR15321351
SRR15321352
SRR15321353
SRR15321354
SRR15321355
SRR15458685
SRR15458686
SRR15458687
SRR15458688
SRR15458689
SRR15458690
SRR15458691
SRR7975946
SRR7975947
SRR7975948
SRR7975949
```

</details>


# Riftia Pangenome assembly  

Samira  

<details>
    <summary>Previous details</summary>
We'll use [Minigraph-Cactus](https://github.com/ComparativeGenomicsToolkit/cactus/blob/master/doc/pangenome.md) for pangenome assembly, which enables reference-based assembly around a chromosome-level genome and incorporates copy number variants (unlike other modern pangenome assembler PGGB).  

Installation (use latest version) - https://github.com/ComparativeGenomicsToolkit/cactus/blob/v2.9.2/README.md  

The documentation is fairly extensive, I recommend reading through it first.  

I would run with `--permissiveContigFilter` to accommodate our less contiguous assemblies, and `--vcf`. 
```
singularity exec /core/labs/Oneill/mneitzey/Software/cactus-2-9-2-gpu.sif --binariesMode singularity cactus-pangenome <jobStorePath> <seqFile> --outDir <output directory> --outName <output file prefix> --reference wsRifPach3-1 --permissiveContigFilter --vcf
```

seqFile (need 4.1 & 5.1):  
```
wsRifPach1-1    /core/projects/EBP/conservation/tubeworms/public_data/Luiz-de-Oliveira_2022/RIFPA_genome_v1/3.1_RIFPA_polished_purged_genome_v1.fasta
wsRifPach2-1    /core/projects/EBP/conservation/tubeworms/public_data/Moggioli_2023/Genome_assemblies/Riftia_pachyptila_genome_assembly_unmasked.fa
wsRifPach3-1    /core/projects/EBP/conservation/tubeworms/genome_assembly/final_assemblies/wsRifPach3.1_genome_chr.fasta
wsRifPach4-1
wsRifPach5-1
```

We may end up running this with and without 5.1 to see the impact.  

~~For pangenome annotation we'll run https://github.com/ComparativeGenomicsToolkit/Comparative-Annotation-Toolkit with our EASEL filtered gff (not longest isoform?).~~  

</details>

Now that we have pangenomes - what's next? Suggested stats and programs below, but please  
- [ ] Gather stats
    - [ ] Number of nodes (bandage)
    - [ ] Number of edges (bandage)
    - [ ] Length of total graph (bandage)
    - [ ] Sequence added per additional genome
    - [ ] Number of structural variants total and per sample (halSummarizeMtuations)
    - [ ] Number of single nucleotide polymorphisms total and per sample (halSnps)
- [ ] Generate images
    - [ ] Growth curve (panacus)

Once the final Rifitia annotations are decided
- [ ] Core genome size (grannot)
- [ ] Private genes (grannot)

<details>
    <summary>GrAnnot installation</summary>

https://forge.ird.fr/diade/dynadiv/grannot & https://hal.science/hal-04490506/document  

```
git clone https://forge.ird.fr/diade/dynadiv/grannot.git
mamba create -n grannot python=3.10  
conda activate grannot
module load bedtools/2.29.0
python3 -m pip install grannot
```
</details>

Lastly, please write the methods and results for your pangenome assemblies, comparing to other existing pangenomes. I would make a tab or two in the manuscript table document to report all stats.   

# Endoriftia assembly  

Andrew  

Working directory: `/core/projects/EBP/conservation/tubeworms/genome_assembly/endoriftia`

For both Riftia and Tevnia we'll start with the reference samples. 

- [x] Build centrifuge database with Endoriftia - see https://gitlab.com/PlantGenomicsLab/pcg-workflows/-/blob/main/workflows/centrifuge/README.md (updated guide from Brandon) & https://www.ncbi.nlm.nih.gov/datasets/genome/GCF_023733635.1/
- [x] Classify Riftia & Tevnia PB & ONT reads
    * Riftia reference / riftia-2 / wsRifPach3.1
        * ONT: `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/ont/03_bamtofastq/Rp_ont_dorado.fastq`
        * PacBio: `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/pb/Rp_pb.fastq.gz`
    * Tevnia reference / tevnia-5 / wsTevJeri1.1
        * There are two sets of ONT reads, one for R9 and one for R10 nanopore flow cells. You can combine the reads later if needed 
            * `/core/projects/EBP/conservation/tubeworms/genome_assembly/tevJeri/reference/02_reads/ont/03_bamtofastq/Tj_ont_dorado_r9.fastq.gz`
            * `/core/projects/EBP/conservation/tubeworms/genome_assembly/tevJeri/reference/02_reads/ont/03_bamtofastq/Tj_ont_dorado_r10.fastq.gz`
        * PacBio: `/seqdata/EBP/animal/invertebrate/tevnia_jerichonana/AD4278_2-5/pacbio/2_B01/Tj_pb1.fastq.gz`   
- [x] Pull out reads classified as Endoriftia  
- [x] Build Endoriftia ~~hybrid~~ genome assemblies with ~~SPADES - https://github.com/ablab/spades~~ Flye
- [x] Evalutate assemblies with quast, busco, merqury  
- [x] Choose 1 assembly per sample to move forward
- [x] Polish ONT assembly with Medaka & QC
- [ ] Ragtag ONT assembly to see if the extra piece belongs, is duplicative, or not  
- [ ] Structurally annotate each with Prokka
- [ ] Functionally annotate each with EnTAP

At this point I think the next stage will be writing. To the manuscript document, add an Endoriftia section. Write separate paragraphs for the methods and results. For the results, describe how these assemblies compare to [previous iterations](https://onlinelibrary.wiley.com/doi/10.1111/1755-0998.13668), including length and gene content. It would be worth looking to see if there are other complete and closed endoriftia genomes. I would make a tab in the manuscript table document to report all stats.  

For next stages / actually using the assemblies, we'll want to find our genes of interest, but that list is still coming (i.e. writing first!). 

After writing, a logical next step would be a pangenome / panproteome of sorts. For this I'd do some research into prokaryotic pangenome tools. Panaroo looks like a good option, which uses Prokka output - https://genomebiology.biomedcentral.com/articles/10.1186/s13059-020-02090-4  


# Writing  

<details>
    <summary>Previous info</summary>
I have shared the manuscript and manuscript folder with you all. - https://drive.google.com/drive/u/0/folders/17jJrzFGsjHYaGJUgVsDwmcM0Cx2_a4_Q  

~~**Manuscript:** Check the Genome Assembly methods in the manuscript (I wrote a first draft). Comment on areas that deviate from your genome assembly process. Everyone should look!~~

**Tables:**
1. Add the raw and decontaminated read data here - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?gid=1650782360#gid=1650782360
1. Add final genome assembly stats here - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?gid=1939787533#gid=1939787533
1. And add final genome assembly stats here - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?gid=1747301490#gid=1747301490 
1. Add stages to final assembly here (not including polishing / any stages that were not moved forward) - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?usp=sharing
1. Add all unused assemblies here (any stages not moved forward and unused preliminary assemblies) - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?usp=sharing
1. Check off assembly stages - https://docs.google.com/spreadsheets/d/1TGUnqDq3QKUoSyebQl30s8-DkKMXUy_l3SmwyRsxz64/edit?usp=sharing  
</details>

# Reading 

I also talked on Friday about data related to Tevnia's microaerophilic tolerance, here are the papers I was referring to:  

* Most specifically this paper - https://tos.org/oceanography/article/chemistry-temperature-and-faunal-distributions-at-diffuse-flow-hydrothermal
* Which is presedented (?) by this paper - https://www.sciencedirect.com/science/article/abs/pii/S0967064509001702?via%3Dihub  

Relevant review paper - "Adaptations to hypoxia in hydrothermal-vent and cold-seep invertebrates" - https://link.springer.com/article/10.1007/s11157-006-9110-3  

Alan  

Regarding gene loss compensation across Vestimentifera (i.e. Vestimentifera genomes):  
* https://academic.oup.com/mbe/article/39/1/msab347/6454105  
* https://www.nature.com/articles/s41467-023-38521-6 (for Oasisia)  
* https://bmcgenomics.biomedcentral.com/articles/10.1186/s12864-023-09166-y  
* https://academic.oup.com/mbe/article/38/10/4116/6320074 (Paraescarpia echinospica)
* Need to sort out - is Endoriftia also a symbiont for Escarpia?  

Let's work on this together? - https://docs.google.com/spreadsheets/d/1L51d6Gayuv4Hb0jyuLlLKQol6AHGZfMT2PD87h0Ng3E/edit?usp=sharing  