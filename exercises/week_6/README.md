# Week 6 (June 25 - July 1)
- Epigenetics
- Genome assembly: basecalling

## Readings
1. **Journal Club -** [Third-Generation Sequencing Reveals the Adaptive Role of the Epigenome in Three Deep-Sea Polychaetes](https://academic.oup.com/mbe/article/40/8/msad172/7231443)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper

1. [Comparative analysis of genome-scale, base-resolution DNA methylation profiles across 580 animal species](https://www.nature.com/articles/s41467-022-34828-y)
1. [The changing face of genome assemblies: Guidance on achieving high-quality reference genomes](https://onlinelibrary.wiley.com/doi/full/10.1111/1755-0998.13312)
1. [Progress, Challenges, and Surprises in Annotating the Human Genome](https://www.annualreviews.org/content/journals/10.1146/annurev-genom-121119-083418)

## Exercises
#### 1. Rebasecall Riftia data
Working with actual research data - yay!  

Michelle is working on the reference, and we also have nanopore sequencing for two additional individuals (`riftia-1_AD4317` and `riftia-5_AD4103`), which we will use for genome assembly and CpG methylation analyses. However, the basecalling is outdated and algorithms have improved read accuracy. So this week you all will work in pairs on re-basecalling the data with Dorado.

> **riftia-1_AD4317** - Andrew & Sia <br>
> **riftia-5_AD4103** - Alan & Samira

**Important Notes:** You can find the software as a module file on Xanadu. To check, type `module avail` into the terminal. To load the software, you will need to add `module load <tool>/<version>` to the top of your bash script.

- **A. Prepare and maintain the git and Xanadu working space**
    - This is the main directory / git repository: `/core/projects/EBP/conservation/tubeworms/`
    - Within there, you'll find:
        - `genome_assembly/rifPach/riftia-1_AD4317`  
        - `genome_assembly/rifPach/riftia-5_AD4103` 
    - Please refer to `genome_assembly/rifPach/reference` for general folder structure and README (noting things are done slightly differently in here - e.g. no rebasecalling)
    - This is the gitlab page: [https://gitlab.com/mneitzey/tubeworms](https://gitlab.com/mneitzey/tubeworms), which I have added everyone to  
    - Please make use of `/core/projects/EBP/conservation/tubeworms/.gitignore` - there will probably be file types / names that need to be added. The git is really only for scripts and READMEs, all other files should be ignored  
- **B. Convert fast5s to pod5s**
    - [Pod5 converter](https://github.com/nanoporetech/pod5-file-format)
    - Fast5 Data
        - riftia-1_AD4317
            - This run was restarted *and* there were "skipped" fast5 files, so there are actually 4 folders to pull fast5s from:
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_skip`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_skip`
        - riftia-5_AD4103
            - This run is simple, 1 folder :) 
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample3/nanopore/2023JAN17_DOG_Rif_5_A4103_PAM25110/2023JAN17_DOG_Rif_5_A4103_PAM25110/20230117_2214_1F_PAM25110_a8900295/fast5_pass`
- **C. Re-basecall with Dorado** 
    - [Dorado basecaller](https://github.com/nanoporetech/dorado)
    - Parameters
        - `--model dna_r10.4.1_e8.2_400bps_sup@v5.0.0`
        - `--modified-bases 5mCG_5hmCG`
        - `--min-qscore 10`
- **D. Convert bam to fastq**
    - [bamtofastq](https://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html)
    - Dorado outputs a bam, which contains methylation that we'll want *later*, but for genome assembly we need the fastqs
- **E. Assess read stats with NanoPlot**
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)
    - Download the report.html and check it out!  

Take your time and double-check / do things correctly rather than quickly - it doesn't all need to be completed this week! 


#### 2. Revisit python
- review previous excercieses from week 1-3
- revisit anaconda installation on xanadu, try and get connected to your jupyter notebook on a computing node.