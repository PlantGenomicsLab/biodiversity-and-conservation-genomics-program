# Week 4 (June 11-17)

- Sequencing & sequencing data
- Git
- Riftia & other Vestimentifera genomes

## Readings
1. [Bioinformatic Data Skills](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/raw/main/readings/week_1/_VinceBuffalo_-_Bioinformatics_Data_Skills_Reproducible_and_Robust_Research_with_Open_Source_Tools_2015__.pdf)
    - Chapter 10: Working with sequencing data
    - Chapter 11 (up to pg. 383 / IGV): Working with alignment data 
    - Chapter 5: Git for scientists
1. **Journal Club -** [Distinct genomic routes underlie transitions to specialised symbiotic lifestyles in deep-sea annelid worms](https://www.nature.com/articles/s41467-023-38521-6)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper

## Exercises

#### 1. [Sequencing Data](https://isg-certificate.github.io/ISG5311/03_0_sequencing_data.html)

Complete modules 7-9

