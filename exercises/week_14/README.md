# Week 14 (September 13 - 19)  

## Select final genomes - Genome groups

As discussed, please share your final genome data with the team slack channel as appropriate for each sample. 

## Trim and Blobtools - Genome groups

Once the final genomes are selected, trim adapters and create SnailPlots and BlobPlots with blobtools. Then look for contamination with blobtools as discussed in the [previous README](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/tree/main/exercises/week_12?ref_type=heads).

## Genome Size Estimation - Sia & Samira

kmerfreq and GCE were created to enable genome size estimation from long-read data. Use the PacBio data for the reference individuals to estimate genome size for both species. 

Riftia: `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/pb/Rp_pb.fastq.gz`  
Tevnia: `/seqdata/EBP/animal/invertebrate/tevnia_jerichonana/AD4278_2-5/pacbio/2_B01/Tj_pb1.fastq.gz`  
Working directory: `/core/projects/EBP/conservation/tubeworms/genome_size_estimation`  

These resources are available for reference:
* [pre-print](https://arxiv.org/abs/2003.11817)
* [GitHub](https://github.com/fanagislab/kmerfreq)
* [Butternut Genome Git](https://gitlab.com/PlantGenomicsLab/butternut-genome-assembly/-/tree/main)

## Mitochondiral analyses - Andrew

Gabrielle Hartley in the O'Neill lab used the PacBio HiFi data from both species as input for [MitoHifi](https://github.com/marcelauliano/MitoHiFi) to create mitochondrial assemblies and annotations for the reference individuals (riftia-2 and tevnia-5). 

The assemblies and annotations live here (final_mitogenome.*, see MitoHifi Github for output info): 

```
/core/projects/EBP/conservation/tubeworms/mt_assembly/MitoHifi
```

Use these assemblies and annotaitons to compare these mitochondiral genomes, specifically the gene order and orientation, with what's been previously published.  

## Genes / gene families of interest - Alan

* Based on our current biological question of "How are Tevnia and Riftia genetically predisposed to different environmental conditions (Tev - low O2, high H2S, high temp)?", find a few genes or gene families that you hypothesize may contribute to these environmental predispositions. Write a brief summary with citations of why you think this gene or gene family is relevant. Find protein sequences for these genes as closely related to Riftia or Tevnia as possible, and add them to `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`. You can also look for other specific genes of interest outside of this question  
* Add summaries here - https://docs.google.com/document/d/1R6yRSlfLBj40RIg-wqcDcqN3mxhoYyXm9W0heB_WaYQ/edit?usp=sharing  
* Add fastas here - `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`
* Add fasta info in "Genes of interest" table here - `/core/projects/EBP/conservation/tubeworms/public_data/README.md`


