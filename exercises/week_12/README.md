# Week 12 (August 6 - 12)  

## Reading

No assigned reading / no journal club this week  

## Mentoring

Meet with your mentor before the start of the school year (ideally by August 16th). Prepare to discuss summer training feedback along with career and program aspirations (and how those can align!).  

* Alan & Brandon
* Andrew & Cynthia
* Sia & Michelle
* Samira & Jill

## Exercises

### 1. Mark brainstorming document

https://docs.google.com/document/d/111Q_Tatr9pYvVe2OYJ93N4zV0_deQn7A37hWBkpwT30/edit

* Checkmark - good idea with some questions/clarifications (add those)
* Star - preferred research item (still comment as appropriate on details of approach - what goes in, what is needed)
* X - do not like - or might require more sequencing or a different set of inputs than we can acquire here

### 2. Finish Riftia assemblies

Clean up your group git README (including scripts, relevant summary tables, and figures).
- Include descriptions on both what happened and also your reflections on the outcomes (bad/good, why?) 
- Here is an example template for this: https://gitlab.com/PlantGenomicsLab/fraxinus-genome-assemblies

Click on the "Details" button below to see assembly guidance:  

<details>

##### 1. Finish rebasecalling Riftia data

> **riftia-1_AD4317** - Andrew & Sia <br>
> **riftia-5_AD4103** - Alan & Samira

**Important Notes:** You can find the software as a module file on Xanadu. To check, type `module avail` into the terminal. To load the software, you will need to add `module load <tool>/<version>` to the top of your bash script.

- **A. Prepare and maintain the git and Xanadu working space**
    - This is the main directory / git repository: `/core/projects/EBP/conservation/tubeworms/`
    - Within there, you'll find:
        - `genome_assembly/rifPach/riftia-1_AD4317`  
        - `genome_assembly/rifPach/riftia-5_AD4103` 
    - Please refer to `genome_assembly/rifPach/reference` for general folder structure and README (noting things are done slightly differently in here - e.g. no rebasecalling)
    - This is the gitlab page: [https://gitlab.com/mneitzey/tubeworms](https://gitlab.com/mneitzey/tubeworms), which I have added everyone to  
    - Please make use of `/core/projects/EBP/conservation/tubeworms/.gitignore` - there will probably be file types / names that need to be added. The git is really only for scripts and READMEs, all other files should be ignored  
- **B. Convert fast5s to pod5s**
    - [Pod5 converter](https://github.com/nanoporetech/pod5-file-format)
    - Fast5 Data
        - riftia-1_AD4317
            - This run was restarted *and* there were "skipped" fast5 files, so there are actually 4 folders to pull fast5s from:
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_skip`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_skip`
        - riftia-5_AD4103
            - This run is simple, 1 folder :) 
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample3/nanopore/2023JAN17_DOG_Rif_5_A4103_PAM25110/2023JAN17_DOG_Rif_5_A4103_PAM25110/20230117_2214_1F_PAM25110_a8900295/fast5_pass`
- **C. Re-basecall with Dorado** 
    - [Dorado basecaller](https://github.com/nanoporetech/dorado)
    - Parameters
        - `--model dna_r10.4.1_e8.2_400bps_sup@v5.0.0`
        - `--modified-bases 5mCG_5hmCG`
        - `--min-qscore 10`
- **D. Convert bam to fastq**
    - [bamtofastq](https://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html)
    - Dorado outputs a bam, which contains methylation that we'll want *later*, but for genome assembly we need the fastqs
- **E. Assess read stats with NanoPlot**
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)
    - Download the report.html and check it out!  

##### 2. Genome assembly with Riftia data
Moving forward, the genome assembly will be split between two methods: one person will decontaminate the reads moving into assembly, and the other will go into assembly with all reads (skip F & G, but help your partner) and decontaminate the contigs at the end of assembly.  
- **F. Identify contaminant reads with Centrifuge**
    - [Centrifuge](https://ccb.jhu.edu/software/centrifuge/manual.shtml)
    - Special parameters
        - `-x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v` - includes bacteria, archael, viral, and human databases  
        - `--exclude-taxids 9606` - to exclude human, since this can throw out metazoan reads  
        - `--min-hitlen 40` - sets minimum base match length to 40
    - Use Recentrifuge on the `.out` file to visualize classification results 
        - [Recentrifuge](https://github.com/khyox/recentrifuge)
- **G. Remove contaminants (classified reads) with Seqkit grep & QC reads with Nanoplot**
    - [Seqkit](https://bioinf.shenwei.me/seqkit/usage/#grep)
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)    
- **H. Assemble genome with Flye**
    - [Flye](https://github.com/mikolmogorov/Flye)
    - Special parameters   
        - `--nano-hq` - since reads were rebasecalled with Dorado
        - `--scaffold`
        - `--no-alt-contigs`
        - `--asm-coverage 60`
- **I. QC genome**
    - Contiguity: [QUAST](https://github.com/ablab/quast)
    - Orthologous gene completeness: [BUSCO](https://busco.ezlab.org/busco_userguide.html)
        - Use `-l /core/labs/Oneill/mneitzey/databases/busco_downloads/lineages/metazoa_odb10` for database
    - Orthologous gene completeness: [Compleasm](https://github.com/huangnengCSU/compleasm)
        - Use singularity file `/core/labs/Oneill/mneitzey/Software/compleasm-0-2-6.sif` since a module isn't available yet
        - Use `-L /core/labs/Oneill/mneitzey/databases/compleasm_db/metazoa_odb10` for database
    - GFA visualization
        - If you have space on your computer, download the .gfa file and open in bandange ([install from here](https://rrwick.github.io/Bandage/))
    - K-mer completeness and correctness: [Merqury](https://github.com/marbl/merqury)
        - Merqury is confusing, learn about it watching this video (can skip trio / diploid parts): [https://www.youtube.com/watch?v=F2wsXEnMP0U](https://www.youtube.com/watch?v=F2wsXEnMP0U)
        - i. Build a k-mer database from the reads using Meryl - you only need to do this once!  
            - Get the gist for how to build one [here](https://github.com/marbl/merqury/wiki/1.-Prepare-meryl-dbs)
            - Copy `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/merqury_dbs/ont_meryl_db.sh` for template script 
        - ii. Run merqury (available on Xanadu)  

##### 3. Genome assembly polishing
Polishing is supposed to improve the base-level accuracy of the genome by mapping the reads back to the assembly and re-calling the most reprepresented base. Lately in our labs polishing hasn't improved assemblies, but this is slightly older data so it's worth a try. The polished assemblies will be compared with the unpolished, and we will decide which (polished or unpolished) to move forward into purging haplotigs.
- **J. Polish genomes with Medaka**
    - [Medaka](https://github.com/nanoporetech/medaka)
    - `--model r1041_e82_400bps_sup_v4.1.0`  
- **K. QC polished genomes**
    - Same steps as I
- **L. Organize results & update README**
    - Organize the QC results between all assemblies so they are easy to compare, we often use Google Sheets
    - Update the README to reflect the steps you've taken so far. See [here](https://gitlab.com/mneitzey/tubeworms/-/blob/master/genome_assembly/rifPach/reference/README.md?ref_type=heads) for an example

##### 4. Genome assembly purging  
To remove extreneous haplotypes from the assembly we'll run Purge Haplotigs.
- **M. Chose whether to move forward with unpolished or polished assemblies**
    - Discuss with your partner then share your QC data and thoughts in the team chat. Michelle will respond to confirm
- **N. Load R dependency for purge haplotigs**
```
# Start an interactive session in Xanadu and load R
module load R/4.2.2

# this will start an R session, which responds to different commands than linux
R

# Install ggplot2. I think you may get asked something about a time zone... I usually just put in whatever is closest (Iowa or something?)
install.packages('ggplot2')

# How to quit R and return to regular linux
quit()
```
- **O. Run Purge Haplotigs**
    - [Purge Haplotigs](https://bitbucket.org/mroachawri/purge_haplotigs/src/master/)
    - [Purge Haplotigs publication](https://bmcbioinformatics.biomedcentral.com/articles/10.1186/s12859-018-2485-7)
    - This consists of 4 steps, starting with mapping the reads used for the assembly (same you would've used for polishing) to the assembly. An important part is after step 2 you'll download the histogram and decide on the coverage cutoffs for low middle and high
    - Find scripts here: `/core/labs/Oneill/mneitzey/Scripts/purge_haplotigs`
- **P. QC purged assemblies**
    - Same steps as I  
- **R. Detect contaminants with Blobtools**
    - Going for this... https://blobtools.readme.io/docs/blobplot - need to figure out the best way to install and run  
- **S. Polish final README**
</details>

### 3. Start Tevnia assemblies

> **tevnia-11_AD4278** - Andrew & Sia <br>
> **tevnia-4_AD4278** - Alan & Samira

This will be pretty much the same process as Riftia - updated paths  

Click on the "Details" button below to see assembly guidance:  

<details>

- **A. Prepare and maintain the git and Xanadu working space**
    - Here are your working directories:
        - `/core/projects/EBP/conservation/tubeworms/genome_assembly/tevJeri/tevnia-11_AD4278`
        - `/core/projects/EBP/conservation/tubeworms/genome_assembly/tevJeri/tevnia-4_AD4278`
    - Create and push README.md docs in your folders (can be blank)  
- **B. Convert fast5s to pod5s**
    - [Pod5 converter](https://github.com/nanoporetech/pod5-file-format)
    - Fast5 Data. Ignore the "84278" in these paths - should be AD4278  
        - tevnia-11_AD4278
            - `/seqdata/EBP/animal/invertebrate/tevnia_jerichonana/TJ-84278_2-11/nanopore/2022OCT25_DOG_TVN_2-11_PAM14134/20221025_1549_1F_PAM14134_8e87fb8a/fast5_pass`
        - tevnia-4_AD4278
            - `/seqdata/EBP/animal/invertebrate/tevnia_jerichonana/TJ-84278_2-4/nanopore/2022OCT25_DOG_TVN_2-4_PAM14350/20221025_1549_1D_PAM14350_ee658f35/fast5_pass`
- **C. Re-basecall with Dorado** 
    - [Dorado basecaller](https://github.com/nanoporetech/dorado)
    - Parameters
        - `--model dna_r10.4.1_e8.2_400bps_sup@v4.1.0`
        - `--modified-bases 5mCG_5hmCG`
        - `--min-qscore 10`
- **D. Convert bam to fastq**
    - [bamtofastq](https://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html)
    - Dorado outputs a bam, which contains methylation that we'll want *later*, but for genome assembly we need the fastqs
- **E. Assess read stats with NanoPlot**
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)
    - Download the report.html and check it out!  

Upcoming: 
* Decontaminate reads + QC
* Assemble + QC
* Polish + QC  
* Purge + QC
* Decontaminate assembly + QC

</details>