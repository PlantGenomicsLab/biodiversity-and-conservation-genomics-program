# Week 2 (May 28 - June 3)

- The first *Riftia* genome 
- HPCs, SLURM, file transfer, file organization & manipulation, intro to python

## Readings
1. **Journal Club -** [Novel Insights on Obligate Symbiont Lifestyle and Adaptation to Chemosynthetic Environment as Revealed by the Giant Tubeworm Genome](https://academic.oup.com/mbe/article/39/1/msab347/6454105)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper
1. [Horizontal endosymbiont transmission in hydrothermal vent tubeworms](https://www.nature.com/articles/nature04793) (Optional)

## Exercises
1. python exercises

    - we did not get through the python examples. I've uploaded the file to the gitlab. It's available as a jupyter notebook (.ipynb) and as an html file. Try and upload the .ipynb file to http://colab.research.google.com. To execute cells, use "shift + enter". The html file is so you can quickly view the notebook in your internet browser (but you can't execute any of the code that way).
        - here is the [notebook](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/blob/main/exercises/week_2/00_bcg_intro_python.ipynb), and [here is the html file](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/blob/main/exercises/week_2/00_bcg_intro_python.html) (you'll need to download it to view it)

    - next read the following (this should be mostly review from what we covered above):
        - Chapter 3 of the official tutorial: https://docs.python.org/3/tutorial/introduction.html

        - Chapter 4 of the official tutorial: https://docs.python.org/3/tutorial/controlflow.html

        - Chapter 5 of the official tutorial: https://docs.python.org/3/tutorial/datastructures.html

        - Do problems 1-3 from [this notebook](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/blob/main/exercises/week_2/Lab7_4100_24.ipynb). First, download the .ipynb file, then upload to google colab.research.google.com for editing : 

1. Review: slurm resource request
    - https://github.com/CBC-UCONN/CBC_Docs/wiki/Requesting-resource-allocations-in-SLURM

1. Before next time: sprucing up your workstation

    - Edit your ssh config file so that you can ssh to xanadu like this `ssh xanadu` AND not be prompted for a password.
        - to do this, re-read and implement the following headers from Ch 4 of [Bioinformatic Data Skills](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/blob/main/readings/week_1/_VinceBuffalo_-_Bioinformatics_Data_Skills_Reproducible_and_Robust_Research_with_Open_Source_Tools_2015__.pdf)
            - `Connecting to Remote Machines with SSH`
            - `Storing Your Frequent SSH Hosts` and
            - `Quick Authentication with SSH Keys`

1. ISG Tutorials [https://isg-certificate.github.io/ISG5311/02_1_scripts.html](https://isg-certificate.github.io/ISG5311/02_1_scripts.html)

    - Complete the following modules:  
        - 4  Scripts and putting it all together
        - 5  SLURM: the job scheduler
        - 6  Data Storage and Software on HPC
1. Script submission practice with [https://github.com/CBC-UCONN/RNA-seq-with-reference-genome-and-annotation](https://github.com/CBC-UCONN/RNA-seq-with-reference-genome-and-annotation)
    1. Clone this tutorial (`git clone`)
    1. Run the first three scripts described
        - Submit, monitor the job, determine how many resources it is using after it completes
        - Be prepared to report on the content in the error and output files from the run