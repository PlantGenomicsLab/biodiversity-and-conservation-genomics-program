# Week 13 (August 30 - September 5)  

## Assembly work

### Non-reference Riftia & Tevnia

* Assembly 
    * Run flye with best dataset & flags and `--nano-raw` *instead of* --nano-hq
        * If stats improve, move into purge haplotigs
    * QC genomes with BUSCO v5.7.1 with Cynthia's singularity container - `/core/labs/Wegrzyn/software/busco-5.7.1.sif`
    * Decide on post-purge best genome with the whole team
    * Decontaminate best genome:
        1. Trim adapters
            * [FCS](https://github.com/ncbi/fcs)
                * [FCS-adaptor](https://github.com/ncbi/fcs/wiki/FCS-adaptor-quickstart)
            1. Make output directory (I called mine `fcs_screen`)
            1. Screen for adaptors with `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_fcs-adaptor/01_fcs_screen.sh`  
            1. Trim adapters with `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_fcs-adaptor/02_fcs_clean.sh`  
        1. Contaminants - [Blobtoolkit blobplot](https://blobtools.readme.io/docs/blobplot)
            1. Can be done all at the same time:
                1. On Xanadu
                    1. (Coverage) - Map reads to genome with winnowmap - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_blobplot/01_winnowmap`
                    1. (Hits) - Blast genome with NCBI database - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_blobplot/02_blast`
                    1. Create blobtools directory for your Assembly - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_blobplot/03_blobtools/01_blobtools_create.sh`
                1. On your local computer (not Xanadu / Mantis)
                    1. Install [docker](https://www.docker.com/) 
                        1. Install blobtoolkit on docker (search for "genomehubs/blobtoolkit" and pull "4.3.11")
            1. Add BUSCO, coverage, and blast hits to blobtools directory - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_blobplot/03_blobtools/02_blobtools_add.sh`
                * If you're curious you can also run `03_blobtk_view.sh` to get snail plot and blob plot png images
            1. Copy blobtools directory to your local computer
            1. Run blobtoolkit image on docker
            1. In the terminal (windows powershell for windows users), enter the following command, filling in the location of your copied blobtools directory (e.g. for Michelle this is `"C:\Users\miche\OneDrive - University of Connecticut\Documents\Oneill\DOG\tubeworms\genome_assembly\rifPach\reference\04_assembly_qc\test_blobplot\03_blobtools\wsRifPach3.1:/blobtoolkit/datasets"`)
                ```
                docker run -d --rm --name btk -v "<local-directory>:/blobtoolkit/datasets" -p 8000:8000 -p 8080:8080 -e VIEWER=true genomehubs/blobtoolkit:latest
                ```
            1. Go to http://localhost:8080/view/all/dataset/datasets/blob?largeFonts=true#Datasets in your local web browser
            1. Identify contigs to remove that look like likely contaminants (high GC content, high coverage, blast to bacteria). Do not remove any eukaryotic contigs  
                * This video should be helpful - https://www.youtube.com/live/T0jbe8gptlk?si=crxLvE5CxaSdRfQw  
            1. Remove contaminated contigs from your assembly and re-QC (with busco, quast, merqury)
* Git 
    * Keep updated
    * Add version numbers for your programs   

## Public data prep

* Other Siboglinidae / outgroup genomes
    1. Find other Siboglinidae and close outgroup genomes
    1. Download the genomes and annotations (ideally gff) to in a appropriately labeled folder `/core/projects/EBP/conservation/tubeworms/public_data`
    1. Add info on the data to `/core/projects/EBP/conservation/tubeworms/public_data/README.md`  
    1. Run BUSCO and QUAST in `/core/projects/EBP/conservation/tubeworms/public_data/qc_public_data`  
    1. Pull longest isoforms and basic stats of annotations from gffs with `/core/projects/EBP/conservation/tubeworms/public_data/format_annotations/agat.sh`
    1. Add QC info to the google table - https://docs.google.com/spreadsheets/d/1kXoazHhpklK_NBuvW88VqfJG2k7FmnyuD-f4B4o1Ck4/edit?usp=sharing  
* Genes / gene families of interest
    * Based on our current biological question of "How are Tevnia and Riftia genetically predisposed to different environmental conditions (Tev - low O2, high H2S, high temp)?", find a few genes or gene families that you hypothesize may contribute to these environmental predispositions. Write a brief summary with citations of why you think this gene or gene family is relevant. Find protein sequences for these genes as closely related to Riftia or Tevnia as possible, and add them to `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`. You can also look for other specific genes of interest outside of this question  
    * Add summaries here - https://docs.google.com/document/d/1R6yRSlfLBj40RIg-wqcDcqN3mxhoYyXm9W0heB_WaYQ/edit?usp=sharing  
    * Add fastas here - `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`
    * Add fasta info in "Genes of interest" table here - `/core/projects/EBP/conservation/tubeworms/public_data/README.md`