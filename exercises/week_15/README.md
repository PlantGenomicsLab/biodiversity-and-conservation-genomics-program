# Week 15? Shared Sept 30, 2024

## Select flye/purge final genomes - Genome groups

As discussed, please share your final genome data with the team slack channel as appropriate for each sample. 

## Clean final genome - Genome groups 

Upon selection of a singular flye/purge genome, please then:

1. Trim adapters with fcs-adaptor & QC
    * [FCS-adaptor documentation](https://github.com/ncbi/fcs/wiki/FCS-adaptor-quickstart)
    * Example scripts - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/07c_adapter-trim/fcs-adaptor`
    * Look at the report - how much adaptor sequence trimmed? how many sequences have adaptors?
    * QC with usual QUAST, BUSCO, Merqury steps
1. Identify and remove contamination with FCS-GX & QC
    * [FCS-GX documentation](https://github.com/ncbi/fcs/wiki/FCS-GX-quickstart)
    * Example scripts - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/09_decontam/fcs-gx`
    * Note that ~ needs to be removed from fcs-adaptor output before running FCS-GX
    * Should be final QC! (QUAST, BUSCO, Merqury)
    * How has the assembly changed over curation? How does the final one compare to previously published assemblies?
1. Remove contigs smaller than 500b
    * [Seqkit seq](https://bioinf.shenwei.me/seqkit/usage/#seq)
    * `seqkit seq --min-len 500 $infasta -o $outfasta`
1. Generate SnailPlot and BlobPlot with blobtools 
    * [Blobplot](https://blobtools.readme.io/docs/blobplot)
    * Example scripts - `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/04_assembly_qc/test_blobplot`
    * `blobtk plot` is where you'll generate the images. I would generate both pngs and svgs (pngs are easy to look at but svgs for publication quality)
    * Try to interpret the plots by comparing with other ones online (can just google snailplot / blobplot in google images) - how do these ones compare? Esp. the blobplot, which tells different information than what we've looked at with BUSCO/QUAST

## Finalize git pages 

You all know what to do!

## Genome Size Estimation - Sia & Samira

Previous text:
```
kmerfreq and GCE were created to enable genome size estimation from long-read data. Use the PacBio data for the reference individuals to estimate genome size for both species. 

Riftia: `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/pb/Rp_pb.fastq.gz`  
Tevnia: `/seqdata/EBP/animal/invertebrate/tevnia_jerichonana/AD4278_2-5/pacbio/2_B01/Tj_pb1.fastq.gz`  
Working directory: `/core/projects/EBP/conservation/tubeworms/genome_size_estimation`  

These resources are available for reference:
* [pre-print](https://arxiv.org/abs/2003.11817)
* [GitHub](https://github.com/fanagislab/kmerfreq)
* [Butternut Genome Git](https://gitlab.com/PlantGenomicsLab/butternut-genome-assembly/-/tree/main)
```

Now that this is complete, please write up a small methods and results section for this information.  

## Mitochondiral analyses - Andrew

Gabrielle Hartley in the O'Neill lab used the PacBio HiFi data from both species as input for [MitoHifi](https://github.com/marcelauliano/MitoHiFi) to create mitochondrial assemblies and annotations for the reference individuals (riftia-2 and tevnia-5). 

The assemblies and annotations live here (final_mitogenome.*, see MitoHifi Github for output info): 

```
/core/projects/EBP/conservation/tubeworms/mt_assembly/MitoHifi
```

Use these assemblies and annotaitons to compare these mitochondiral genomes, specifically the gene order and orientation, with what's been previously published.  

## Genes / gene families of interest - Alan

Previous info:
```
* Based on our current biological question of "How are Tevnia and Riftia genetically predisposed to different environmental conditions (Tev - low O2, high H2S, high temp)?", find a few genes or gene families that you hypothesize may contribute to these environmental predispositions. Write a brief summary with citations of why you think this gene or gene family is relevant. Find protein sequences for these genes as closely related to Riftia or Tevnia as possible, and add them to `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`. You can also look for other specific genes of interest outside of this question  
* Add summaries here - https://docs.google.com/document/d/1R6yRSlfLBj40RIg-wqcDcqN3mxhoYyXm9W0heB_WaYQ/edit?usp=sharing  
* Add fastas here - `/core/projects/EBP/conservation/tubeworms/public_data/genes_of_interest`
* Add fasta info in "Genes of interest" table here - `/core/projects/EBP/conservation/tubeworms/public_data/README.md`
```

Now that you have some genes of interest, learn more about each - what data is available? What info is known about these genes/families within Riftia/Tevnia/Siboglinidae/Vestimentifera/Annelida (wherever it makes sense to expand to)? What expectations do you have about these when comparing Riftia, Tevnia, and other species (gene family expansion / contraction, positive or negative selection?)?