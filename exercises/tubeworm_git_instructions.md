# Tubeworm Git

Gitlab: https://gitlab.com/mneitzey/tubeworms  

On Xanadu: `/core/projects/EBP/conservation/tubeworms`  

A couple of overall notes:
* We're only *pushing* to git from Xanadu, not pulling, for simplicity - i.e. only make changes on Xanadu, not on GitLab
* Only READMEs (README.md and associated images) and scripts (*.sh, maybe *.py or *.R) should be committed to the git
* Definitely ask for help if you run into any issues!

## Set-up git on Xanadu

Follow [slides 10-11 here](https://docs.google.com/presentation/d/1yuVCPYA0WhKo3fzLpHrAzpw6I8Y_XE6FiceJYC2R1SA/edit#slide=id.g2e66320b821_0_204) to set up your ssh key in Xanadu and connect it to gitlab. You'll need to do this in order to push to gitlab.  

## Instructions for pushing git

Instead of individuals, you and your genome teammate are now responsible for updating your scripts on git. 

1. Make an announcement in the #conservation-genomics-team Slack channel that you are making git updates. Things can get very hairy if multiple people are working with git at the same time  
1. Enter the folder you want to updated 
1. Check git status of your respective folder with `git status .`
1. Add files with `git add <path>` 
1. Commit with a message - e.g. `git commit -m "Add Riftia1 flye script"`
1. Push to the git with `git push origin main`. Wait until it is finished  
1. Take a look at https://gitlab.com/mneitzey/tubeworms if you would like to see the updates in action
1. Inform the #conservation-genomics-team channel that you're done with git  

# August 28, 2024 updates

## Branches

The default branch is now **main** instead of master. You may need to switch from master to main with `git checkout -b main`, and you'll push with `git push origin main`

## .gitignore

`/core/projects/EBP/conservation/tubeworms/.gitignore`  

The .gitignore is now set to ignore all file types (`*.*`) by default, *except* `*.sh` files and `README.md`. If there is a file you need to add that is not a .sh or README.md (e.g. an image), please add it to the gitignore with `!` and the specific path, e.g. `!genome_assembly/rifPach/reference/image.png`  