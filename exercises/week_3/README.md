# Week 3 (June 4 - June 11)
 
- Data transfer with FTP client
- File manipulation with python, sed, awk and grep

## Readings

Next week we will meet in ESB 205 to learn about sequencing technology (10-11am discussion; 11am-12pm tour with Nicole). Sequencing technology moves faster than publications, so for this next week we'll rely on the sequencing technology product learning for "reading". We've provided a number of links, but feel free to follow any others that you want to know more about. Watching videos highly encouraged!

1. 2nd generation / short-read sequencing
    * Illumina
        1. [https://www.illumina.com/science/technology/next-generation-sequencing/beginners.html](https://www.illumina.com/science/technology/next-generation-sequencing/beginners.html)
        1. [https://www.illumina.com/science/technology/next-generation-sequencing/beginners/ngs-workflow.html](https://www.illumina.com/science/technology/next-generation-sequencing/beginners/ngs-workflow.html)
        1. [https://www.illumina.com/science/technology/next-generation-sequencing/beginners/applications.html](https://www.illumina.com/science/technology/next-generation-sequencing/beginners/applications.html)
        1. [https://www.illumina.com/science/technology/next-generation-sequencing/sequencing-technology.html](https://www.illumina.com/science/technology/next-generation-sequencing/sequencing-technology.html)
        1. [https://www.illumina.com/science/technology/next-generation-sequencing/illumina-sequencing-history.html](https://www.illumina.com/science/technology/next-generation-sequencing/illumina-sequencing-history.html)
1. 3rd generation / long-read sequencing
    * Nanopore
        1. [https://nanoporetech.com/platform/](https://nanoporetech.com/platform/)
        1. [https://nanoporetech.com/platform/technology](https://nanoporetech.com/platform/technology)
        1. [https://nanoporetech.com/platform/technology/flow-cells-and-nanopores](https://nanoporetech.com/platform/technology/flow-cells-and-nanopores)
        1. [https://nanoporetech.com/platform/technology/basecalling](https://nanoporetech.com/platform/technology/basecalling)
        1. [https://nanoporetech.com/platform/accuracy](https://nanoporetech.com/platform/accuracy)
        1. [https://nanoporetech.com/platform/accuracy/simplex](https://nanoporetech.com/platform/accuracy/simplex)
        1. [https://nanoporetech.com/platform/accuracy/duplex](https://nanoporetech.com/platform/accuracy/duplex)
    * Pacbio
        1. [https://www.pacb.com/learn/sequencing-101/](https://www.pacb.com/learn/sequencing-101/)
        1. [https://www.pacb.com/blog/long-read-sequencing/](https://www.pacb.com/blog/long-read-sequencing/)
        1. [https://www.pacb.com/blog/sbb-sequencing/](https://www.pacb.com/blog/sbb-sequencing/)
        1. [https://www.pacb.com/blog/understanding-accuracy-in-dna-sequencing/](https://www.pacb.com/blog/understanding-accuracy-in-dna-sequencing/)
        1. [https://www.pacb.com/blog/steps-of-smrt-sequencing/](https://www.pacb.com/blog/steps-of-smrt-sequencing/)

## Exercises
For the following exercises, you can start an [interactive session](https://bioinformatics.uconn.edu/resources-and-events/tutorials-2/xanadu/#Xanadu_srun) on Xanadu and run within the terminal:

<pre style>
srun --partition=general --qos=general --mem=4G --pty bash
</pre>
#### 1. [Sed/AWK and Regular Expressions](https://eriqande.github.io/eca-bioinf-handbook/sed-awk-and-regular-expressions.html)
- Read through **chapter 6** of ECA's Bioinformatics handbook
- Copy exercise files into a `week_3` working directory:

**STEP 1:** Enter the directory (if it does not exist, mkdir)
<pre style>
cd $HOME/bcg/week_3
</pre>

**STEP 2:** Pull the exercise files from online with `git clone`.
<pre style>
git clone https://github.com/eriqande/awk-and-sed-inputs
cd awk-and-sed-inputs
</pre>

#### 2. [Grep](https://learnbyexample.github.io/learn_gnugrep_ripgrep/Exercise_solutions.html)
- Same as above, you will git clone a repository with files into `$HOME/bcg/week_3`:
<pre style>
git clone https://github.com/learnbyexample/learn_gnugrep_ripgrep.git
cd exercises
</pre>

#### 3. python

- review dict, pd.Series, pd.DataFrame
    - review these classes in class notes in `01_pandas.ipynb`
    - read and execute the lesson in `01_Lab9_4100_24.ipynb`
    - there is also an html version of each file you can view in your browser after downloading
- a few more pandas exercises will be uploaded in the afternoon on June 4

#### 4. Anaconda
- follow [these instructions that I wrote](https://drk-lo.github.io/lotterhoslabprotocols/bioinformagics_4_anaconda.html) to set up an Anaconda environment on xanadu - note that these instructions are intended for a cluster named `discovery` - just apply the instructions to `xanadu`.
    - use the file `py312.yml` to create your conda environment (with python v3.12) using `conda env create --name py312 --file py312.yml`

### 5. Launch and connect to jupyter notebooks on the cluster
- follow [these instructions](https://drk-lo.github.io/lotterhoslabprotocols/bioinformagics_6_jupyter.html) - note these instructions are for a cluster named `discovery`, I think the only thing that will need to change is the name of the login nodes that are used - so don't use `login-01` use e.g., `hpc-ext-1` (there are 5 login nodes)
    - your notebook.sh file for xanadu should look like this:
    ```bash
    echo "launching on port $1"

    ssh -N -f -R $1:localhost:$1 hpc-ext-1
    ssh -N -f -R $1:localhost:$1 hpc-ext-2
    ssh -N -f -R $1:localhost:$1 hpc-ext-3
    ssh -N -f -R $1:localhost:$1 hpc-ext-4
    ssh -N -f -R $1:localhost:$1 hpc-ext-5

    cd $HOME/code
    jupyter notebook --no-browser --port $1
    ```
    - note you will need to `mkdir -p $HOME/code`. the `notebook.sh` script will launch jupyter in that directory, and this is where you should store your code.

### 6. For fun: color code your terminal
- follow [these instructions](https://drk-lo.github.io/lotterhoslabprotocols/bioinformagics_7_terminal_colors.html) to make your files and terminal colorful