# Week 7 (July 2-8 )
- Genome annotation

## Readings
1. **Journal Club -** [Progress, Challenges, and Surprises in Annotating the Human Genome](https://www.annualreviews.org/content/journals/10.1146/annurev-genom-121119-083418) - yes this was on last week's list, this week we'll discuss :)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper
2. [Genome annotation: From human genetics to biodiversity genomics](https://www.cell.com/cell-genomics/fulltext/S2666-979X(23)00172-6)
3. Pangenomics intro: [Every base everywhere all at once: pangenomics comes of age](https://www.nature.com/articles/d41586-023-01300-w)

## Exercises
#### 1. Genome annotation exercises
The following [slide deck](https://docs.google.com/presentation/d/1bwS_NGs6qAhxftSH-Vqg6bEWRqabTTNyvmf2oa5tuP4/edit?usp=sharing) contains exercises related to:
- GFF/GTF file manipulation and interpretation
- BAM file format
- Introduction to IGV 

#### 2. Finish rebasecalling Riftia data

> **riftia-1_AD4317** - Andrew & Sia <br>
> **riftia-5_AD4103** - Alan & Samira

**Important Notes:** You can find the software as a module file on Xanadu. To check, type `module avail` into the terminal. To load the software, you will need to add `module load <tool>/<version>` to the top of your bash script.

- **A. Prepare and maintain the git and Xanadu working space**
    - This is the main directory / git repository: `/core/projects/EBP/conservation/tubeworms/`
    - Within there, you'll find:
        - `genome_assembly/rifPach/riftia-1_AD4317`  
        - `genome_assembly/rifPach/riftia-5_AD4103` 
    - Please refer to `genome_assembly/rifPach/reference` for general folder structure and README (noting things are done slightly differently in here - e.g. no rebasecalling)
    - This is the gitlab page: [https://gitlab.com/mneitzey/tubeworms](https://gitlab.com/mneitzey/tubeworms), which I have added everyone to  
    - Please make use of `/core/projects/EBP/conservation/tubeworms/.gitignore` - there will probably be file types / names that need to be added. The git is really only for scripts and READMEs, all other files should be ignored  
- **B. Convert fast5s to pod5s**
    - [Pod5 converter](https://github.com/nanoporetech/pod5-file-format)
    - Fast5 Data
        - riftia-1_AD4317
            - This run was restarted *and* there were "skipped" fast5 files, so there are actually 4 folders to pull fast5s from:
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081/20220906_1951_3F_PAM14081_6fbfe965/fast5_skip`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_pass`
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample2/nanopore/2022SEP06_DOG_rif-mon_114m_PAM14081/2022SEP06_DOG_rif-mon_114m_PAM14081_b/20220907_1434_3F_PAM14081_6bc1a3c5/fast5_skip`
        - riftia-5_AD4103
            - This run is simple, 1 folder :) 
                - `/seqdata/EBP/animal/invertebrate/riftia_pachyptila/sample3/nanopore/2023JAN17_DOG_Rif_5_A4103_PAM25110/2023JAN17_DOG_Rif_5_A4103_PAM25110/20230117_2214_1F_PAM25110_a8900295/fast5_pass`
- **C. Re-basecall with Dorado** 
    - [Dorado basecaller](https://github.com/nanoporetech/dorado)
    - Parameters
        - `--model dna_r10.4.1_e8.2_400bps_sup@v5.0.0`
        - `--modified-bases 5mCG_5hmCG`
        - `--min-qscore 10`
- **D. Convert bam to fastq**
    - [bamtofastq](https://bedtools.readthedocs.io/en/latest/content/tools/bamtofastq.html)
    - Dorado outputs a bam, which contains methylation that we'll want *later*, but for genome assembly we need the fastqs
- **E. Assess read stats with NanoPlot**
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)
    - Download the report.html and check it out!  

#### 3. Genome assembly with Riftia data
Moving forward, the genome assembly will be split between two methods: one person will decontaminate the reads moving into assembly, and the other will go into assembly with all reads (skip F & G, but help your partner) and decontaminate the contigs at the end of assembly.  
- **F. Identify contaminant reads with Centrifuge**
    - [Centrifuge](https://ccb.jhu.edu/software/centrifuge/manual.shtml)
    - Special parameters
        - `-x /isg/shared/databases/centrifuge/b+a+v+h/p_compressed+h+v` - includes bacteria, archael, viral, and human databases  
        - `--exclude-taxids 9606` - to exclude human, since this can throw out metazoan reads  
        - `--min-hitlen 40` - sets minimum base match length to 40
    - Use Recentrifuge on the `.out` file to visualize classification results 
        - [Recentrifuge](https://github.com/khyox/recentrifuge)
- **G. Remove contaminants (classified reads) with Seqkit grep & QC reads with Nanoplot**
    - [Seqkit](https://bioinf.shenwei.me/seqkit/usage/#grep)
    - [Nanoplot](https://github.com/wdecoster/NanoPlot)    
- **H. Assemble genome with Flye**
    - [Flye](https://github.com/mikolmogorov/Flye)
    - Special parameters   
        - `--nano-hq` - since reads were rebasecalled with Dorado
        - `--scaffold`
        - `--no-alt-contigs`
        - `--asm-coverage 60`
- **I. QC genome**
    - Contiguity: [QUAST](https://github.com/ablab/quast)
    - Orthologous gene completeness: [BUSCO](https://busco.ezlab.org/busco_userguide.html)
        - Use `-l /core/labs/Oneill/mneitzey/databases/busco_downloads/lineages/metazoa_odb10` for database
    - Orthologous gene completeness: [Compleasm](https://github.com/huangnengCSU/compleasm)
        - Use singularity file `/core/labs/Oneill/mneitzey/Software/compleasm-0-2-6.sif` since a module isn't available yet
        - Use `-L /core/labs/Oneill/mneitzey/databases/compleasm_db/metazoa_odb10` for database
    - GFA visualization
        - If you have space on your computer, download the .gfa file and open in bandange ([install from here](https://rrwick.github.io/Bandage/))
    - K-mer completeness and correctness: [Merqury](https://github.com/marbl/merqury)
        - Merqury is confusing, learn about it watching this video (can skip trio / diploid parts): [https://www.youtube.com/watch?v=F2wsXEnMP0U](https://www.youtube.com/watch?v=F2wsXEnMP0U)
        - i. Build a k-mer database from the reads using Meryl
            - Get the gist for how to build one [here](https://github.com/marbl/merqury/wiki/1.-Prepare-meryl-dbs)
            - Copy `/core/projects/EBP/conservation/tubeworms/genome_assembly/rifPach/reference/02_reads/merqury_dbs/ont_meryl_db.sh` for template script 
        - ii. Run merqury (available on Xanadu)  