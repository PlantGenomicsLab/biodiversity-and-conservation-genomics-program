# Week 1 (May 21-28)

- Introduction to conservation genomics
- How to read papers and prepare for journal club
- Navigating the terminal and accessing Xanadu (HPC)
- [Complete pre-course survey](https://uconn.co1.qualtrics.com/jfe/form/SV_ey6C7GYi9VkSo1o)

## Readings
1. **Journal Club -** [How to Read a Scientific Paper](https://blog.aspb.org/wp-content/uploads/2016/08/HowtoReadScientificPaper.pdf?_gl=1*ycqnhl*_up*MQ..*_ga*NDMyMDg2MzE1LjE3MTU4NjM1NDg.*_ga_S6WC3B1CF7*MTcxNTg2MzU0Ny4xLjAuMTcxNTg2MzU0Ny4wLjAuMA..)
1. [The History of Life at Hydrothermal Vents](https://www.sciencedirect.com/science/article/pii/S0012825221001021#s0020) (Introduction Only)
1. [New Perspectives on the Ecology and Evolution of Siboglinid Tubeworms](https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0016309)
1. [Chapters 1-4 : Bioinformatic Data Skills](https://gitlab.com/PlantGenomicsLab/biodiversity-and-conservation-genomics-program/-/raw/main/readings/week_1/_VinceBuffalo_-_Bioinformatics_Data_Skills_Reproducible_and_Robust_Research_with_Open_Source_Tools_2015__.pdf)
1. **Journal Club -** [Prokaryotic Cells in the Hydrothermal Vent Tube Worm Riftia pachyptila Jones: Possible Chemoautotrophic Symbionts](Cavanaugh_ProkaryoticCellsHydrothermal_1981.pdf)
    - For each major section - abstract, intro, results (+ results subsections) and for each figure - using bullet points write out the main take-home messages.
    - Find: title, author affiliations and contact info, journal name / web address, abstract & intro sections, permanent record number (Digital Object Identifier), publication record dates, editor name
    - After you've read the paper, describe future directions / yet-unknowns identified by the authors.
    - Think of questions you have about the paper

## Videos
1. [Episode 1: The Deep Sea Environment](https://www.youtube.com/watch?v=QUF1_Fjo--w&list=PLMvtEOr--T7NfHS7F22xDDb4OA2lHRNAC)
1. [Episode 5: Hydrothermal Vents](https://www.youtube.com/watch?v=by33sVQDlSg&list=PLMvtEOr--T7NfHS7F22xDDb4OA2lHRNAC&index=4)
1. [Episode 6: Life in the Deep Sea](https://www.youtube.com/watch?v=zKuVbbAKDvg&list=PLMvtEOr--T7NfHS7F22xDDb4OA2lHRNAC&index=5)


## Exercises
All exercises will be completed on the Xanadu cluster in the following directory:
<pre style>
/home/FCAM/username/bcg
</pre>

#### Logging onto Xanadu
> **MacOS:** Open the app “Terminal” on your computer <br>
> **Windows:** Install [WSL](https://learn.microsoft.com/en-us/windows/wsl/setup/environment) and open the Ubuntu terminal window

To login to Xanadu, paste the following path into your terminal (changing username):

<pre style>
ssh username@xanadu-submit-ext.cam.uchc.edu
</pre>

Next, provide your password and press enter. You will not see any text pop-up when you start typing your password, but don't worry! 

---

#### 1. [The Shell Part 1](https://drive.google.com/file/d/1o8u8dBB9RNcx_dF0n-aue6OHDwFoyrSN/view)
The following files will be used to complete this exercise:
> animals.txt <br>
> the_raven.txt <br>
> Wonderful_world.txt <br>
> Thoreau_quotes.txt

#### **Copying the files into a `bcg` working directory:** <br>
**STEP 1:** Enter your home directory 
<pre style>
cd $HOME
</pre>

**STEP 2:** Confirm that you are in your home directory (you should see: `/home/FCAM/username`)
<pre style>
pwd
</pre>
**STEP 3:** Create a _new_ directory called `bcg`
<pre style>
mkdir bcg
</pre>

**STEP 4:** Enter the `bcg` directory using a relative path
<pre style>
cd bcg
</pre>

**STEP 5:** Recursively (`-r`) copy the [week_1](exercises/week_1) exercises into your working directory (`/home/FCAM/username/bcg`) using a relative path (`.`)
<pre style>
cp -r /core/projects/EBP/conservation/bcg/exercises/week_1 .
</pre>

#### 2. [Intro to Linux and Bash](https://isg-certificate.github.io/ISG5311/01_0_IntroToLinux.html)
Complete the following modules:

- Getting Started with Linux
- Working with Files

#### 3. [Linux Journey](https://linuxjourney.com/) (Optional)
