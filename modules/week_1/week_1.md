# Introduction to Terminal and Coding Exercises

Objectives:
1. Open the terminal on your computer
2. Navigate directories
3. Understand absolute vs. relative paths
4. Edit text files with vim

## Exercise 1: Open the Terminal on Your Computer
**Operating system (OS):** software that controls the operation of a computer and directs the processing of the user's programs

1. **Windows**: Press `Windows Key` to open the Start menu, type `Windows Terminal`, and press Enter. 
2. **macOS**: Press `Cmd + Space` to open Spotlight Search, type `Terminal`, and press Enter. 
3. **Linux**: Press `Ctrl + Alt + T`. Alternatively, search for "Terminal".

## Exercise 2: Navigate Directories

#### Basic Linux Commands
When in a terminal, you can use the following commands to navigate, create, view, redirect and download directories (folders) and files:

| Command | Description                              | Example                                    |
|---------|------------------------------------------|--------------------------------------------|
| `cd`    | Navigate through directories             | `cd /path/to/directory`                    |
| `ls`    | List directories and files               | `ls`                                       |
| `touch` | Create an empty file                     | `touch newfile.txt`                        |
| `mkdir` | Create a directory                       | `mkdir newdirectory`                       |
| `cat`   | Display file contents or concatenate     | `cat file.txt`                             |
| `less`  | Display paged outputs                    | `less file.txt`                            |
| `head`  | View the first few lines of a file       | `head file.txt`                            |
| `tail`  | View the bottom few lines of a file      | `tail file.txt`                            |
| `mv`    | Rename or move files                     | `mv oldname.txt newname.txt`               |
| `rm`    | Delete files or directories              | `rm file.txt`                              |
| `cp`    | Copy files or directories                | `cp source.txt destination.txt`            |
| `chmod` | Change file permissions                  | `chmod 755 script.sh`                      |
| `wget`  | Download files from the internet         | `wget http://example.com/file.zip`         |
| `grep`  | Search for a string within an output     | `grep "search term" file.txt`              |

While this list does not include all of the linux commands available to you, it does include those which you will use most frequently. 

### Exercise 1: Printing a Working Directory

Print the path of the following directory on your computer: `/Users/yourusername`

<details>
<summary>Answer</summary>
pwd /Users/yourusername
</details>

### Exercise 2: Listing Files and Directories

Lists the files and directories in `/Users/yourusername`

<details>
<summary>Answer</summary>
ls /Users/yourusername
</details>

### Exercise 3: Changing Directories

#### Absolute vs. Relative Paths
**Absolute path:** Provides the complete and specific location of a file or directory from the root directory of the file system. It includes all the directories and subdirectories leading to the desired file or directory.
  - Example:
  ```sh
  /home/username/Documents/file.txt
  ```

**Relative Path:** Specifies the location of a file or directory relative to the current working directory or another reference point. It does not start from the root directory but instead uses the context of the current directory.

  - Example:
  ```sh
  Current working directory: /home/username/Documents
  Relative path: file.txt
  ```

Navigate to your `/Users/yourusername/Downloads` directory (this may differ for Mac and Windows users)

<details>
<summary>Answer</summary>
cd /Users/yourusername/Downloads
</details>

Navigate to your `/Users/yourusername` directory and use a relative path to move to `Downloads`

<details>
<summary>Answer</summary>
cd /Users/yourusername
cd Downloads
</details>

**Hint:** you can move back a directory with “..”

- Move back one directory:
  cd ..

- Move back two directories:
  cd ../..

- Move back three directories:
  cd ../../..

Move back **one** directory from `/Users/yourusername/Downloads`, what command do you use to see the path?

<details>
<summary>Answer</summary>
cd ..
pwd
</details>

**IMPORTANT NOTES:**
  - When in doubt, use the **absolute path!**
  - **SPELLING MATTERS!** Error messages may be long and confusing, but it’s fairly easy to spot a path error: `File provided doesn't exist or the path is incorrect`
  - Troubleshooting: Check that the path you provide a script exists by typing `ls /path/to/file`

## Exercise 4: Creating a Directory
On many computers, **directories** are known as **folders**. Navigate to: `/Users/yourusername/Downloads` and create the following directory `Conservation`

<details>
<summary>Answer</summary>
cd /Users/yourusername/Downloads
mkdir Conservation
</details>

## Exercise 5: Creating a File

#### Filename Extensions
While you do not NEED one, all files generally have a filename **extension:**
**.txt** - text
**.csv** - comma-separated
**.tsv** - tab separated
… there are many examples …
**.fasta** - text-based format for representing either nucleotide sequences or amino acid (protein) sequences

In `/Users/yourusername/Downloads/Conservation` create the following empty file: `file.txt`

<details>
<summary>Answer</summary>
cd /Users/yourusername/Downloads/Conservation
touch file.txt
</details>


### Exercise 6: Opening a File in vim

Open the file you just created, `file.txt`, using `vim`

<details>
<summary>Answer</summary>
vim file.txt
</details>

#### Basic vim Commands
- **Insert Mode**: Press `i` to enter insert mode and start editing the text.
  - **Example**: 
    ```vim
    i
    (now you can type and edit the text)
    ```

- **Save and Exit**: Press `Esc` to exit insert mode, then type `:wq` and press Enter.
  - **Example**: 
    ```vim
    Esc :q Enter
    ```

Type `"Hello World"` into the `file.txt`, save, and exit.

<details>
<summary>Answer</summary>
i
Hellow World
:x
</details>

### Exercise 7: Display a File

View `file.txt` 

<details>
<summary>Answer 1</summary>
less file.txt
</details>

<details>
<summary>Answer 2</summary>
cat file.txt
</details>


<details>
<summary>Answer 3</summary>
head file.txt
</details>

### Exercise 8: Rename or Move Files and Directories

Create a new directory called `Archive` and move a new txt file called `trash.txt` there 

<details>
<summary>Answer</summary>
mkdir Archive
touch trash.txt
mv trash.txt Archive/
</details>

In the `Conservation` directory you created, rename `file.txt` to `new_name.txt`

<details>
<summary>Answer</summary>
mv file.txt new_name.txt
</details>

### Exercise 9: Delete a File
**BE CAREFUL!!!!!**
  - Double check that you are in the directory you intend to be in with `pwd`
  - Make sure the file you are deleting is indeed the file you want to delete

Remove `new_name.txt` in the `Conservation` directory

<details>
<summary>Answer</summary>
rm new_name.txt
</details>
