<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Gather form data
    $name = $_POST['name'];
    // Gather more form data...

    // Email setup
    $to = "cynthia.webster@uconn.edu";
    $subject = "UConn Mentee Pre-Survey Submission";
    $message = "Name: $name\n"; // Add more form data to the message...

    // Send email
    if (mail($to, $subject, $message)) {
        echo "Form submitted successfully. Thank you!";
    } else {
        echo "Error: Unable to submit form.";
    }
}
?>
